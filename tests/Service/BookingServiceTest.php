<?php

namespace App\Tests\Service;

use App\Entity\Booking;
use App\Entity\Employee;
use App\Entity\Seat;
use App\Repository\BookingRepository;
use App\Service\BookingService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BookingServiceTest extends TestCase
{
    /**
     * @var Booking|MockObject $existingBooking
     */
    protected MockObject|Booking $existingBooking;

    /**
     * @var Booking|MockObject $newBooking
     */
    protected MockObject|Booking $newBooking;
    /**
     * @var BookingRepository|MockObject
     */
    private mixed $bookingRepository;
    /**
     * @var EntityManager|MockObject
     */
    private mixed $em;

    protected function setUp(): void
    {
        $this->em                = $this->createMock(EntityManager::class);
        $this->bookingRepository = $this->createMock(BookingRepository::class);
        $this->existingBooking   = $this->createMock(Booking::class);
        $this->newBooking        = $this->createMock(Booking::class);
    }

    /**
     * @return Seat|MockObject
     */
    private function makeSeat(): Seat|MockObject
    {
        return $this->createMock(Seat::class);
    }

    /**
     * @return Employee|MockObject
     */
    private function makeEmployee(): Employee|MockObject
    {
        return $this->createMock(Employee::class);
    }

    public function testMakeBookingDaySuccess()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 08:00:00');
        $end      = new \DateTime('2016-12-22 17:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertTrue($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::MAKE_BOOKING_SUCCESS, $bookingService->getMessage());
    }

    public function testMakeBookingHourSuccess()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 10:00:00');
        $end      = new \DateTime('2016-12-22 11:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertTrue($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::MAKE_BOOKING_SUCCESS, $bookingService->getMessage());
    }

    public function testMakeBookingDoubleBookedEmployee()
    {
        $this->bookingRepository->method('findByEmployeeAndTime')->willReturn(array(['result' => 1]));
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee   = $this->makeEmployee();
        $seat       = $this->makeSeat();
        $secondSeat = $this->makeSeat();
        $start      = new \DateTime('2016-12-22 10:00:00');
        $end        = new \DateTime('2016-12-22 11:00:00');
        $this->setBooking($this->newBooking, $start,$end,$secondSeat,$employee);
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::EMPLOYEE_DOUBLE_BOOKED, $bookingService->getMessage());

    }

    public function testMakeBookingDoubleBookedSeat()
    {
        $this->bookingRepository->method('findBySeatAndTime')->willReturn(array(['result' => 1]));
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee       = $this->makeEmployee();
        $secondEmployee = $this->makeEmployee();
        $seat           = $this->makeSeat();
        $start          = new \DateTime('2016-12-22 10:00:00');
        $end            = new \DateTime('2016-12-22 11:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$secondEmployee);

        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::SEAT_DOUBLE_BOOKED, $bookingService->getMessage());
    }

    public function testMakeBookingInvalidTime()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 11:00:00');
        $end      = new \DateTime('2016-12-22 10:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::START_END_TIME_INVALID, $bookingService->getMessage());
    }

    public function testMakeBookingInvalidStart()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 05:00:00');
        $end      = new \DateTime('2016-12-22 06:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::START_TIME_NOT_VALID, $bookingService->getMessage());
    }

    public function testMakeBookingInvalidEnd()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 20:00:00');
        $end      = new \DateTime('2016-12-22 21:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::END_TIME_NOT_VALID, $bookingService->getMessage());
    }

    public function testMakeBookingInvalidLength()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        //booking length must not be either 1 or 9 hours
        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 10:00:00');
        $end      = new \DateTime('2016-12-22 13:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);#
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::BOOKING_LENGTH_INVALID, $bookingService->getMessage());
    }

    public function testMakeBookingMultipleDays()
    {
        $bookingService = new BookingService($this->em, $this->bookingRepository);

        $employee = $this->makeEmployee();
        $seat     = $this->makeSeat();
        $start    = new \DateTime('2016-12-22 10:00:00');
        $end      = new \DateTime('2016-12-23 11:00:00');
        $this->setBooking($this->newBooking, $start,$end,$seat,$employee);
        self::assertFalse($bookingService->makeBooking($this->newBooking));
        self::assertEquals($bookingService::BOOKING_DAYS_MUST_MATCH, $bookingService->getMessage());
    }

    private function setBooking(MockObject|Booking $booking, \DateTime $startTime, \DateTime $endTime, Seat $seat, Employee $employee)
    {
        $booking->method('getSeat')->willReturn($seat);
        $booking->method('getEmployee')->willReturn($employee);
        $booking->method('getStartTime')->willReturn($startTime);
        $booking->method('getEndTime')->willReturn($endTime);
    }
}
