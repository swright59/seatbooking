<?php

namespace App\Tests\Service;

use App\Entity\Office;
use App\Service\OfficeSeatService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class OfficeSeatServiceTest extends TestCase
{
    /**
     * @var OfficeSeatService $officeSeatService
     */
    private OfficeSeatService $officeSeatService;

    protected function setUp(): void
    {
        $em                      = $this->createMock(EntityManager::class);
        $this->officeSeatService = new OfficeSeatService($em);
    }

    public function testCreateCalculateSeatEntityNewOffice()
    {
        $office = $this->createMock(Office::class);
        $this->officeSeatService->setOffice($office);

        self::assertTrue( $this->officeSeatService->createCalculateSeatEntity(20));
    }

    /**
     * Here are some samples of what the rest of the tests could be for this service

    public function testCreateCalculateSeatEntityOfficeAddSeats()
    {
        //this has not been properly implemented, yet
        //We would test that when we increase the number of seats in the office, that the collection of seats changes.
    }

    public function testCreateCalculateSeatEntityOfficeRemove()
    {
        //this has not been properly implemented, yet
        //We would test that when we reduce the number of seats in the office, that the collection of seats changes.
    }
     */
}
