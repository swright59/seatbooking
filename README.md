SeatBooker
=======
----

this is a sample project, allowing for the management of employees, offices, seats allowing for employees to book seats within certain offices.

## Specification
- PHP 8.0 / Symfony 5
- nginx
- mysql


---
# INSTALL
requirements:

- docker

---
## RUN
from inside docker folder

```bash
    docker-compose up -d
```

then, if you would like to run in prod mode, from project root (run one at a time)
```bash
    composer install
    php bin/console doctrine:migrations:migrate 
```
If you would instead like to run this in dev mode, with the profiler, from project root (run one at a time)
```bash
        composer install --dev
        php bin/console doctrine:migrations:migrate 
```

For unit tests
```bash
    Use the configuration defined in the file phpunit.xml.dist
    Use the docker container as the interpreter.    
```

and finally, open your web browser and navigate to http://localhost:8080, you will see the main page of the application.

#Further notes

It would have been better to create some data fixtures for this project, but it is workable without.

We could have put validation around some more entity fields, however this wasn't in the requirements.

There is some repeated code across the AvailabilityType and the BookingType classes, we would rather use a js widget and leave it as a front-end task to provide
a nice UI to the user, but otherwise we should refactor out the duplication.

We have provided a way to check availability for a specific time and office, however it would be best to create an API that can handle this request, the
front-end could query this API whenever a user updates their selection while creating a booking, allowing for more up-to-date information to be provided to the
user.

We have created an interface and abstracted some booking logic. This is to allow for multiple types of bookings in the future, e.g. we could extend this to
meeting rooms, this wasn't expanded upon as it was beyond the scope of the project.

Some additional functionality over and above the specification has been provided, as the functionality was simple and allows for easier testing of the UI, it
was left in.

There may be a race condition where employees may be able to book the same seat at the same time as each other. We did not want to over engineer the
solution at this stage.

UI elements would be better paginated.


