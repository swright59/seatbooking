<?php

namespace App\Form\Model;

use App\Entity\Office;

class OfficeAvailabilityFormModel
{
    public Office $office;
    public \DateTimeInterface $startTime;
    public \DateTimeInterface $endTime;
}
