<?php

namespace App\Form;

use App\Entity\Booking;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class BookingType extends AbstractType
{
    /**
     * We may choose to use a javascript datepicker here
     * but for now, HTML5 will give us a suitably nice picker.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        //todo: abstract this functionality away from this form + availability type to get rid of duplicate code.
        //Really we should use a plugin for displaying these date fields. UX wise there is room for improvement.
        //the below values without doing these weird calculations, we can configure this form builder to use a js widget.
        $currentYear = intval(date("Y"));
        $years[] = $currentYear;
        if(date("m") > 10) {
            $years[] = $currentYear + 1;
        }

        $builder
            ->add('startTime', DateTimeType::class, [
                'widget' => 'choice',
                'with_minutes'=> false,
                'hours' => [8,9,10,11,12,13,14,15,16,17],
                'years' => $years,
            ])
            ->add('endTime', DateTimeType::class, [
                'widget' => 'choice',
                'with_minutes'=> false,
                'hours' => [8,9,10,11,12,13,14,15,16,17],
                'years' => $years,
            ])
            ->add('seat', EntityType::class, [
                'class' => 'App:Seat',
                'choice_label' => function ($seat) {
                    return $seat->getOffice()->getDescription() . ' ' . $seat->getPosition();
                }
            ])
            ->add('employee', EntityType::class, [
                'class' => 'App:Employee',
                'choice_label' => 'payrollNumber',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'booking_array' => array(),
        ]);
    }
}
