<?php

namespace App\Service;

use App\Entity\AbstractBooking;

interface BookingInterface
{
    /**
     * @param AbstractBooking $booking
     *
     * @return bool
     */
    public function makeBooking(AbstractBooking $booking): bool;
}