<?php

namespace App\Service;

use App\Entity\Office;
use App\Entity\Seat;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class OfficeSeatService
{

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var Office $office
     */
    private Office $office;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    /**
     * @param int $numberOfSeats
     * @param Office|null $prevOffice
     *
     * @return bool
     */
    public function createCalculateSeatEntity(int $numberOfSeats, Office $prevOffice = null): bool
    {
        if (null === $prevOffice) {
            $result = $this->addSeats($numberOfSeats);
        } elseif ($this->office->getSeats()->count() > $prevOffice->getSeats()->count()) {
            //There is no requirement to increase the number of seats in the office. The following line
            //should work to do this, if the requirement is ever needed.
            $result = true;
            //$result = $this->addSeats($this->office->getSeats()->count() - $prevOffice->getSeats()->count());
        } else {
            $result = true;
            //We would use this to remove seats, if the office has fewer seats than it used to.
            //This is not a requirement as so has been skipped.
        }
        return $result;
    }

    /**
     * @param int $numberOfAddSeats
     *
     * @return bool
     */
    private function addSeats(int $numberOfAddSeats): bool
    {
        for ($i = 1; $i <= $numberOfAddSeats ; $i++) {
            $seat = new Seat();
            $seat->setPosition($i);
            $this->office->addSeat($seat);
            $this->em->persist($seat);
        }
        try {
            $this->em->flush();
        } catch (\Doctrine\ORM\Exception\ORMException $e) {
            return false;
        }
        return true;
    }
}