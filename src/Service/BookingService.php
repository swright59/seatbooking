<?php

namespace App\Service;

use App\Entity\AbstractBooking;
use App\Entity\Booking;
use App\Entity\Office;
use App\Form\Model\OfficeAvailabilityFormModel;
use App\Repository\BookingRepository;
use App\Repository\SeatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class BookingService extends AbstractBookingService
{
    public const START_END_TIME_INVALID  = 'The start time cannot be after the end time.';
    public const START_TIME_NOT_VALID    = 'The start time is invalid, it must be greater than 08:00.';
    public const END_TIME_NOT_VALID      = 'The end time is invalid, it must be less than 17:00';
    public const EMPLOYEE_INVALID        = 'The employee booking this does not exist.';
    public const SEAT_INVALID            = 'This seat does not exist';
    public const EMPLOYEE_DOUBLE_BOOKED  = 'This employee already has a booking for this time.';
    public const SEAT_DOUBLE_BOOKED      = 'This seat is already booked for this time frame.';
    public const MAKE_BOOKING_SUCCESS    = 'Booking successful';

    public const BOOKING_LENGTH_INVALID  = 'The booking length is invalid, it must be either 1 hour or 9 hours.';
    public const BOOKING_DAYS_MUST_MATCH = 'The start and end of the booking must fall on the same day.';

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var BookingRepository
     */
    private BookingRepository $bookingRepository;

    /**
     * @param EntityManagerInterface $em
     * @param BookingRepository $bookingRepository
     */
    public function __construct(EntityManagerInterface $em, BookingRepository $bookingRepository)
    {
        $this->em = $em;
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * @param Booking|AbstractBooking $booking
     *
     * @return bool
=     */
    public function makeBooking(Booking|AbstractBooking $booking): bool
    {
        if($this->checkValidBooking($booking)){
            $this->message = self::MAKE_BOOKING_SUCCESS;
            //$employee      = $booking->getEmployee();
            //$employee->addBooking($booking);
            $this->em->persist($booking);
            $this->em->flush($booking);
            //$this->em->persist($employee);
            //$this->em->flush($employee);
            return true;
        }
        return false;
    }

    /**
     * @param Booking|AbstractBooking $booking
     *
     * @return bool
     */
    protected function checkValidBooking(Booking|AbstractBooking $booking): bool
    {
        $employee  = $booking->getEmployee();
        $seat      = $booking->getSeat();
        $startTime = $booking->getStartTime()->format("H");
        $endTime   = $booking->getEndTime()->format("H");
        $interval  = $booking->getStartTime()->diff($booking->getEndTime());
        $hours     = $endTime - $startTime;

        if (!$employee) {
            $this->message = self::EMPLOYEE_INVALID;
            return false;
        }

        if (!$seat) {
            $this->message = self::SEAT_INVALID;
            return false;
        }

        if ($booking->getStartTime() > $booking->getEndTime()) {
            $this->message = self::START_END_TIME_INVALID;
            return false;
        }

        //Check that the booking start and end is on the same day.
        if (0 != $interval->d || 0 != $interval->m || 0 != $interval->y){
            $this->message = self::BOOKING_DAYS_MUST_MATCH;
            return false;
        }

        if (8 > $startTime) {
            $this->message = self::START_TIME_NOT_VALID;
            return false;
        }

        if (18 < $endTime) {
            $this->message = self::END_TIME_NOT_VALID;
            return false;
        }

        //Check that the time period being booked is either 9 or 1 hours.
        if (9 != $hours && 1 != $hours) {
            $this->message = self::BOOKING_LENGTH_INVALID;
            return false;
        }

        //Check if this employee has already booked a seat for this time slot.
        if(!empty($this->bookingRepository->findByEmployeeAndTime($booking->getEmployee(), $booking->getStartTime(), $booking->getEndTime()))) {
            $this->message = self::EMPLOYEE_DOUBLE_BOOKED;
            return false;
        }

        //Check if the specific seat has already been booked for this time slot
        if(!empty($this->bookingRepository->findBySeatAndTime($booking->getSeat(), $booking->getStartTime(), $booking->getEndTime()))) {
            $this->message = self::SEAT_DOUBLE_BOOKED;
            return false;
        }
        return true;
    }

    public function checkAvailabilityForTime(OfficeAvailabilityFormModel $availabilityFormModel, SeatRepository $seatRepository): array
    {
        $officeSeats = $seatRepository->findBy(['office' => $availabilityFormModel->office]);
        $test        = new ArrayCollection($this->bookingRepository->findAllBookedSeatsForTimeAndOffice($availabilityFormModel->startTime, $availabilityFormModel->endTime, $officeSeats));

        //If there are no bookings, we know every seat in the office is available.
        if(0 === $test->count()){
            $nonBookedSeats = $officeSeats;
        }else{
            $nonBookedSeats = $seatRepository->findByOfficeSeatsExceptBooked($availabilityFormModel->office, $test);
        }
        return $nonBookedSeats;
    }
}