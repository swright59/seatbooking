<?php

namespace App\Service;

use App\Entity\AbstractBooking;

abstract class AbstractBookingService implements BookingInterface
{
    /**
     * We could store an array here and choose to not fail fast.
     * @var string
     */
    protected string $message;

    /**
     * @param AbstractBooking $booking
     *
     * @return mixed
     */
    abstract protected function checkValidBooking(AbstractBooking $booking): bool;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}