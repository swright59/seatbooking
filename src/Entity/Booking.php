<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking extends AbstractBooking
{

    /**
     * @ORM\ManyToOne(targetEntity=Seat::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Seat $seat;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class, inversedBy="bookings", cascade={"persist"})
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", nullable="false")
     */
    protected Employee $employee;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Seat
     */
    public function getSeat(): Seat
    {
        return $this->seat;
    }

    /**
     * @param Seat $seat
     *
     * @return $this
     */
    public function setSeat(Seat $seat): self
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employeeId
     *
     * @return $this
     */
    public function setEmployee(Employee $employeeId): self
    {
        $this->employee = $employeeId;

        return $this;
    }
}
