<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use App\Repository\SeatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * We may choose to extend this class in the future, e.g. with co-ordinates or floor level, to allow users to book
 * specific seats, for now we are using 'position' to act as all of this info.
 * @ORM\Entity(repositoryClass=SeatRepository::class)
 */
class Seat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Office::class, inversedBy="seats", cascade={"persist"})
     * @ORM\JoinColumn(name="office_id", referencedColumnName="id", nullable="false")
     */
    protected Office $office;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $position = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Office|null
     */
    public function getOffice(): ?Office
    {
        return $this->office;
    }

    /**
     * @param Office $office
     *
     * @return $this
     */
    public function setOffice(Office $office): self
    {
        $this->office = $office;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
