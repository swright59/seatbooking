<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractBooking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected ?int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected \DateTimeInterface $startTime;

    /**
     * @ORM\Column(type="datetime")
     */
    protected \DateTimeInterface $endTime;

    /**
     * @return \DateTimeInterface
     */
    public function getStartTime(): \DateTimeInterface
    {
        return $this->startTime;
    }

    /**
     * @param \DateTimeInterface $startTime
     *
     * @return $this
     */
    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEndTime(): \DateTimeInterface
    {
        return $this->endTime;
    }

    /**
     * @param \DateTimeInterface $endTime
     *
     * @return $this
     */
    public function setEndTime(\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }
}