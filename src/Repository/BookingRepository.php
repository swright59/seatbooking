<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Seat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Symfony\Component\String\s;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param Seat $seat
     * @param \DateTimeInterface $startTime
     * @param \DateTimeInterface $endTime
     *
     * @return int|mixed|string
     */
    public function findBySeatAndTime(Seat $seat, \DateTimeInterface $startTime, \DateTimeInterface $endTime)
    {
        return $this->createQueryBuilder('b')
            ->Where('b.seat = :seat')
            ->setParameter('seat', $seat)
            ->andWhere('b.startTime = :startTime')
            ->setParameter('startTime', $startTime)
            ->andWhere('b.endTime = :endTime')
            ->setParameter('endTime', $endTime)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllBookedSeatsForTimeAndOffice(\DateTimeInterface $startTime, \DateTimeInterface $endTime, ?array $officeSeats)
    {
        return $this->createQueryBuilder('b')
            ->Where('b.startTime >= :startTime')
            ->setParameter('startTime', $startTime )
            ->andWhere('b.endTime <= :endTime')
            ->setParameter('endTime', $endTime )
            ->andWhere('b.seat IN (:seat)')
            ->setParameter('seat', $officeSeats)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param Employee $employee
     * @param \DateTimeInterface|null $startTime
     * @param \DateTimeInterface|null $endTime
     *
     * @return int|mixed|string
     */
    public function findByEmployeeAndTime(Employee $employee, ?\DateTimeInterface $startTime, ?\DateTimeInterface $endTime)
    {
        return $this->createQueryBuilder('b')
            ->Where('b.employee = :employee')
            ->setParameter('employee', $employee)
            ->andWhere('b.startTime = :startTime')
            ->setParameter('startTime', $startTime)
            ->andWhere('b.endTime = :endTime')
            ->setParameter('endTime', $endTime)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Booking[] Returns an array of Booking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Booking
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
