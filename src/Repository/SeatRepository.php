<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Office;
use App\Entity\Seat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Seat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seat[]    findAll()
 * @method Seat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeatRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Seat::class);
    }

    public function findByOfficeSeatsExceptBooked(Office $office, ArrayCollection $bookings)
    {
        $seats = array();
        /** @var Booking $booking */
        foreach($bookings as $booking){
            $seats[] = $booking->getSeat();
        }
        return $this->createQueryBuilder('s')
            ->Where('s.office >= :office')
            ->setParameter('office', $office )
            ->andWhere('s NOT IN (:seat)')
            ->setParameter('seat', $seats)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Seat[] Returns an array of Seat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Seat
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
