<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Office;
use App\Entity\Seat;
use App\Form\AvailabilityType;
use App\Form\BookingType;
use App\Form\Model\OfficeAvailabilityFormModel;
use App\Repository\BookingRepository;
use App\Repository\SeatRepository;
use App\Service\BookingService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/booking')]
class BookingController extends AbstractController
{
    #[Route('/', name: 'booking_index', methods: ['GET'])]
    public function index(BookingRepository $bookingRepository): Response
    {
        return $this->render('booking/index.html.twig', [
            'bookings' => $bookingRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'booking_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BookingService $bookingService, BookingRepository $bookingRepository): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $result = $bookingService->makeBooking($booking);
            } catch (\Exception $e) {
                //log the exception, cloudwatch would be a good choice.
                var_dump($e);
                $result = false;
            }

            if($result){
                return $this->render('booking/index.html.twig', [
                    'bookings' => $bookingRepository->findAll(),
                    'message'  => $bookingService->getMessage()
                ]);
            }
            return $this->render('booking/new.html.twig', [
                'booking' => $booking,
                'form'    => $form->createView(),
                'message' => $bookingService->getMessage(),
            ]);
        }

        return $this->render('booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    /**
     * I think this would make more sense as an API request that the front end calls.
     * Instead of the user making a request to see all booked seats for a specific time, we could have the front-end
     * query our endpoint any time a new time slot is selected on a new booking page.
     * This would mean the user is always getting up-to-date data.
     *
     * @param Request $request
     * @param BookingService $bookingService
     * @param SeatRepository $seatRepository
     *
     * @return Response
     */
    #[Route('/checkAvailability', methods: ['GET', 'POST'])]
    public function checkSeats(Request $request, BookingService $bookingService, SeatRepository $seatRepository): Response
    {
        $form = $this->createForm(AvailabilityType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var OfficeAvailabilityFormModel $availabilityModel */
            $availabilityModel = $form->getData();
            $nonBookedSeats    = $bookingService->checkAvailabilityForTime($availabilityModel, $seatRepository);

            return $this->render('booking/availability_form.html.twig', [
                'seats' => $nonBookedSeats
            ]);
        }

        return $this->render('booking/availability.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'booking_show', methods: ['GET'])]
    public function show(Booking $booking): Response
    {
        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
        ]);
    }

    #[Route('/{id}/edit', name: 'booking_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Booking $booking): Response
    {
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('booking_index');
        }

        return $this->render('booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'booking_delete', methods: ['POST'])]
    public function delete(Request $request, Booking $booking): Response
    {
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute('booking_index');
    }
}
